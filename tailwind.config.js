/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  theme: {
    extend: {
      screens: {
        "-2xl": {
          max: "1535px",
        },
        "-xl": {
          max: "1279px",
        },
        "-lg": {
          max: "1023px",
        },
        "-md": {
          max: "767px",
        },
        "-sm": {
          max: "639px",
        },
      },
      fontSize: {
        22: "22px",
        25: "25px",
        27: "27px",
        35: "35px",
        30: "30px",
        37: "37px",
        43: "43px",
        70: "70px",
        75: "75px",
        80: "80px",
        100: "100px",
      },
      margin: {
        30: "30px",
        35: "35px",
        47: "47px",
        50: "50px",
        66: "66px",
        71: "71px",
        99: "99px",
        162: "162px",
        176: "176px",
        183: "183px",
        189: "189px",
        190: "190px",
      },
      padding: {
        38: "38px",
        112: "112px",

      },
      textColor: {
        blue: "#3C82F4",
        darkBlue: "#3D82F2",
        gray: "#707070",
        green: "#0FC65C"
      },
      backgroundColor: {
        blue: "#306EF7",
        gray: "#2D2D2D"
      }
    },
  },
  plugins: [],
};
