
export const routes = {
  HOME: "/",
  ABOUT: "/about",
  PRICING: "/pricing",
  BLOG: "/blog",
  LOGIN: "/login",
  SIGNUP: "/signup",
};
