import { Home } from "./components";
import { Route, Routes } from "react-router-dom";
import { routes } from "./constant";
function App() {
  return (
    <div className="App">
      <Routes>
      <Route path={routes.HOME} element={<Home />} />
      </Routes>
    </div>
  );
}

export default App;
