import React from 'react'
import {Navbar, Hero, WhyMarico, Step1, Step2, Step3, ExpertsAgree, GetStarted, Footer} from "./index";
const Home = () => {
  return (
    <div>
      <Navbar />
      <Hero />
      <WhyMarico />
      <Step1 />
      <Step2 />
      <Step3 />
      <ExpertsAgree />
      <GetStarted />
      <Footer />
    </div>
  )
}

export default Home
