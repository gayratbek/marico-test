import React, { useState } from "react";
import logo from "./assets/img/logo.png";
import { Link } from "react-router-dom";
import { routes } from "../constant";
const data = [
  {
    title: "case 1",
    to: "/case1",
  },
  {
    title: "case 2",
    to: "/case2",
  },
  {
    title: "case 3",
    to: "/case3",
  },
];
const Navbar = () => {
  const [navBtn, setNavBtn] = useState(false);
  return (
    <nav className={!navBtn ? "mt-50":"h-[95px] w-full fixed pt-[50px] top-0 left-0 bg-black"}>
      <div className="container">
        <div className="box text-xl flex justify-between items-center">
          <div className="left">
            <Link to={routes.HOME} className="flex items-center">
              <img src={logo} alt="logo" />
              <span className="font-extrabold text-37 ml-4">Marico</span>
            </Link>
          </div>
          <div className="center text-gray ">
            <ul className="flex items- justify-center gap-x-[38px] -lg:hidden">
              <li className="flex items-center hover:text-blue cursor-pointer relative useCases">
                Use Cases{" "}
                <span className="pt-1">
                  {" "}
                  <i className="bx bx-chevron-down"></i>{" "}
                </span>{" "}
                <ul className="useCases-child hidden absolute top-8 right-0 w-14 h-20">
                  {data?.map((item, id) => (
                    <li
                      key={id}
                      className="text-gray hover:text-blue whitespace-nowrap"
                    >
                      <Link to={item?.to}>{item?.title}</Link>
                    </li>
                  ))}
                </ul>
              </li>
              <li>
                <Link to={routes.ABOUT} className="hover:text-blue">
                  About
                </Link>
              </li>
              <li>
                <Link to={routes.PRICING} className="hover:text-blue">
                  Pricing
                </Link>
              </li>
              <li>
                <Link to={routes.BLOG} className="hover:text-blue">
                  Blog
                </Link>
              </li>
            </ul>
            <button onClick={() => setNavBtn(!navBtn)} className="text-3xl hidden -lg:block text-white">
              {!navBtn && <i className="bx bx-menu"></i>}
              {navBtn && <i className="bx bx-x"></i>}
            </button>
            {navBtn && (
              <div className="flex justify-center items-start fixed top-[93px] left-0 w-full mobile-navbar bg-black pt-[10%]">
                <ul className="flex items-center gap-x-[38px] flex-col gap-y-4">
                  <li className="flex items-center hover:text-blue cursor-pointer relative useCases">
                    Use Cases{" "}
                    <span className="pt-1">
                      {" "}
                      <i className="bx bx-chevron-down"></i>{" "}
                    </span>{" "}
                    <ul className="useCases-child hidden absolute top-8 right-0 w-[120px] h-20 bg-black">
                      {data?.map((item, id) => (
                        <li
                          key={id}
                          className="text-gray hover:text-blue whitespace-nowrap"
                        >
                          <Link to={item?.to}>{item?.title}</Link>
                        </li>
                      ))}
                    </ul>
                  </li>
                  <li>
                    <Link to={routes.ABOUT} className="hover:text-blue">
                      About
                    </Link>
                  </li>
                  <li>
                    <Link to={routes.PRICING} className="hover:text-blue">
                      Pricing
                    </Link>
                  </li>
                  <li>
                    <Link to={routes.BLOG} className="hover:text-blue">
                      Blog
                    </Link>
                  </li>
                  <li>
                    <Link to={routes.LOGIN} className="hover:text-blue">
                      <button>Login</button>
                    </Link>
                  </li>
                  <li>
                    <Link to={routes.SIGNUP}>
                      <button className="bg-blue sign-up-btn text-white">
                        Sign Up
                      </button>
                    </Link>
                  </li>
                </ul>
              </div>
            )}
          </div>
          <div className="right text-gray flex items-center -lg:hidden">
            <Link to={routes.LOGIN}>
              <button>Login</button>
            </Link>
            <Link to={routes.SIGNUP}>
              <button className="bg-blue sign-up-btn text-white ml-6">
                Sign Up
              </button>
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
