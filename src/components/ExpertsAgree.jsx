import React from "react";
import topImg from "./assets/img/experts-top.png";
const ExpertsAgree = () => {
  return (
    <section>
      <div className="top mx-47 mb-190 -2xl:mb-40 -xl:mb-24 -sm:mb-5 -sm:mt-8 -sm:overflow-hidden">
        <h2 className="text-75 mb-[52px] text-center -2xl:text-[60px] -xl:text-4xl -sm:text-3xl -sm:mb-16">Experts Agree</h2>
        <img src={topImg} alt="Error" className="-sm:object-cover -sm:scale-[1.8]" />
      </div>
      <div className="expertsAgree-bottom bottom flex justify-center items-center h-[799px] w-full object-cover -md:h-[599px] -sm:h-[350px]">
        <h2 className="text-70 w-[70%] text-center -2xl:text-[60px] -xl:text-4xl -sm:text-3xl -xl:w-full -xl:mx-[15px]">
          We've helped over 1,000 creatorsre claim control of their audience.
        </h2>
      </div>
    </section>
  );
};

export default ExpertsAgree;
