import React from "react";
import img from "./assets/img/step2-mobile.png";
const Step2 = () => {
  return (
    <section>
      <div className="top mt-71 -md:mt-8 -sm:mt-8">
        <div className="container">
          <div className="box flex justify-center items-center">
            <div className="flex items-center flex-col w-3/5 text-center -xl:w-full">
              <span className="text-darkBlue text-25 -xl:text-xl">STEP 2</span>
              <h2 className="text-[75px] -2xl:text-[60px] -md:text-4xl -sm:text-3xl">Share Your Homepage</h2>
              <span className="text-30 text-gray -2xl:text-2xl -md:text-xl -sm:text-base">
                Share your Wavium homepage link with your followers, and we'll
                handle the rest.
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="bottom mt-176 -2xl:mt-36 -md:mt-24 -sm:mt-5">
        <div className="container">
          <div className="box flex justify-between gap-[23px] -md:flex-col">
            <div className="left w-1/2 bg-[#0D0D0D] -2xl:overflow-hidden -md:w-full">
              <div className="pt-[33px] pl-[45px]">
                <span className="pb-6 block text-green">One Link</span>
                <div className="text-80 -2xl:text-[60px] -lg:text-4xl">
                <h2 className=" text-gray">ALL You Create.</h2>
                <h2 className="">One Link</h2>
                </div>
                <img src={img} alt="Error" className="ml-[112px]" />
              </div>
            </div>
            <div className="right w-1/2 bg-[#0D0D0D] -md:w-full">
              <div className="pt-[33px] pl-[45px]">
                <span className="pb-6 block text-green">
                  Collect Subscribers
                </span>
                <div className="-2xl:text-[60px] -lg:text-4xl">
                <h2 className=" text-gray w-2/3">Emails. Phone #s.</h2>
                <h2 className="-md:mb-2.5">All Yours.</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="get mt-12 flex items-center justify-center mb-189 -2xl:mb-40 -md:mb-20 -sm:mb-8 -sm:mt-5">
        <button className="bg-blue rounded-xl step2-get-btn">
        Get Started 
        </button>
      </div>
    </section>
  );
};

export default Step2;
