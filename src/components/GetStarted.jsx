import React from "react";

const GetStarted = () => {
  return (
    <section className="mt-190 mb-[210px] -2xl:my-40 -xl:my-mb-99 -md:my-16 -sm:py-5">
      <div className="container">
        <div className="box flex flex-col items-center text-center justify-center">
          <svg
            width="165"
            height="155"
            viewBox="0 0 165 155"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M0 85.6504V154.767H59.863L59.851 144.989L0 85.6504Z"
              fill="#326CF9"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M104.576 154.78H164.439V85.6611L104.59 145.003L104.576 154.78Z"
              fill="#326CF9"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M164.439 0.347168L82.395 81.6919L0 0V70.291L82.395 151.983L164.427 70.6499L164.439 0.347168Z"
              fill="#326CF9"
            />
          </svg>
          <h1 className="text-100 -2xl:text-[60px] -md:text-5xl -sm:text-3xl -md:mt-2.5">Get Started Now</h1>
          <span className="text-43 mb-4 -xl:text-3xl -md:text-base -sm:text-sm -sm:text-center -sm:mx-[15px]">
            Setup is easy and takes under 5 minutes.
          </span>
          <button className="bg-[#326CF9] px-112 py-7 rounded-2xl -xl:mt-47 -md:text-base -xl:px-20 -xl:py-5 -sm:px-10 -sm:py-3 -sm:mt-3">
            Get Started Now
          </button>
          <div className="bottom text-[25px] flex items-center mt-5 -md:text-2xl -sm:text-sm">
            <svg
              width="37"
              height="37"
              viewBox="0 0 37 37"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M18.8292 7.76562C24.9396 7.76562 29.8931 12.7192 29.8931 18.8296C29.8931 24.9399 24.9396 29.8936 18.8292 29.8936C12.7186 29.8936 7.76514 24.9399 7.76514 18.8296C7.76514 12.7192 12.7186 7.76562 18.8292 7.76562Z"
                fill="#0FC65C"
              />
              <path
                opacity="0.24"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M18.5 0C28.7173 0 37 8.28271 37 18.5C37 28.7173 28.7173 37 18.5 37C8.28271 37 0 28.7173 0 18.5C0 8.28271 8.28271 0 18.5 0Z"
                fill="#0FC65C"
              />
            </svg>
            <span className="mx-2.5">1000+</span>
            <span className="text-gray">creators have already started</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default GetStarted;
