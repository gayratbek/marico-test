import React from "react";
const Hero = () => {
  return (
    <section>
      <div className="container">
        <div className="box flex flex-col items-center">
          <div className="top text-center text-[112px]  mt-[128px] -2xl:mt-99 -2xl:text-80 -lg:text-[60px] -md:text-[48px] -sm:text-3xl -sm:mt-8">
            <h1 className="">Own your audience.</h1>
            <h1 className="linear-gradient font-extrabold ">
              So you don't lose them.
            </h1>
          </div>
          <div className="center mt-[130px] w-1/2 flex items-center flex-col justify-center text-center mb-[188px] -2xl:my-99 -xl:w-2/3 -md:w-full -sm:my-8">
            <p className="text-[40px] w-[90%] mb-50 -2xl:text-30 -md:text-2xl -sm:text-lg -sm:mb-5">
              Turn your audience into email and text message subscribers.
            </p>
            <div className="flex mb-5">
              <button className="bg-blue rounded-lg hero-btn mr-[26px]">
                Get Started Now
              </button>
              <button className="rounded-lg hero-btn hero-btn-right text-gray">
                View A Demo
              </button>
            </div>
            <div className="bottom text-[25px] flex items-center -md:text-2xl -sm:text-sm">
              <svg
                width="37"
                height="37"
                viewBox="0 0 37 37"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M18.8292 7.76562C24.9396 7.76562 29.8931 12.7192 29.8931 18.8296C29.8931 24.9399 24.9396 29.8936 18.8292 29.8936C12.7186 29.8936 7.76514 24.9399 7.76514 18.8296C7.76514 12.7192 12.7186 7.76562 18.8292 7.76562Z"
                  fill="#0FC65C"
                />
                <path
                  opacity="0.24"
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M18.5 0C28.7173 0 37 8.28271 37 18.5C37 28.7173 28.7173 37 18.5 37C8.28271 37 0 28.7173 0 18.5C0 8.28271 8.28271 0 18.5 0Z"
                  fill="#0FC65C"
                />
              </svg>
              <span className="mx-2.5">1000+</span>
              <span className="text-gray">creators have already started</span>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
