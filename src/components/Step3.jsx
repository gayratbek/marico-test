import React from "react";
import img from "./assets/img/step3-right.png";
const data = [
  "Embed content or create something new to share.",
  "Share content over email, text message or your homepage.",
];
const Step3 = () => {
  return (
    <section className="mb-99 -2xl:mb-71 -xl:mb-12 -sm:mb-5">
      <div className="top">
        <div className="container">
          <div className="box flex justify-center items-center">
            <div className="flex items-center flex-col w-3/5 text-center -xl:w-full">
              <span className="text-darkBlue text-25 -xl:text-xl">STEP 3</span>
              <h2 className="text-[75px] -2xl:text-[60px] -md:text-4xl -sm:text-3xl">Send Emails & Text Messages</h2>
              <span className="text-30 text-gray -2xl:text-2xl -md:text-xl -sm:text-base">
                No more going through a social platform. Reach and engage your
                audience directly over email and text massage.
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="bottom mt-190 ml-183 flex justify-between -xl:flex-col -xl:ml-0 -2xl:mt-71 -md:mt-5">
        <div className="left w-1/2 -xl:w-full flex flex-col -xl:items-center">
          <p className="text-30 mb-20 -xl:mb-3 -md:text-2xl -sm:text-xl">Your Homepage</p>
          <div className="text-75 -2xl:text-[60px] -md:text-5xl -sm:text-3xl -sm:text-center">
          <h2 className="">Reach Your</h2>
          <h2 className=" mb-30 -sm:mb-3">
            Audience <span className="text-blue"> Directly. </span>
          </h2>
          </div>
          <div className="flex flex-col gap-y-3 -md:text-base -sm:text-sm -sm:text-center -sm:mx-[15px]">
            {data?.map((item, id) => (
              <div key={id} className="flex items-center -sm:flex-col">
                <div className="bg-gray w-[33px] h-[33px] rounded-full flex items-center justify-center text-lg">
                  <span className="mt-1">{id + 1} </span>
                </div>
                <span className="ml-3">{item} </span>
              </div>
            ))}
          </div>
          <div className="buttons flex mt-66 text-xl -xl:mt-47 -md:text-base">
            <button className="step1-left-btn bg-blue rounded-xl mr-6">
              Get Started Now
            </button>
            <button className="step1-right-btn hero-btn-right rounded-xl text-gray bg-[#0E0E0E]">
              View A Demo
            </button>
          </div>
        </div>
        <div className="right w-1/2 -xl:w-full flex flex-col">
          <img src={img} alt="" className="-xl:w-[95%]" />
        </div>
      </div>
    </section>
  );
};

export default Step3;
