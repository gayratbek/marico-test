import React from "react";
import logo from "./assets/img/logo.png";
import { Link } from "react-router-dom";
import { routes } from "../constant";

const Footer = () => {
  return (
    <footer className="mt-50">
      <div className="container">
        <div className="box text-xl flex justify-between items-center -md:flex-col -md:gap-y-5">
          <div className="left">
            <Link to={routes.HOME} className="flex items-center">
              <img src={logo} alt="logo" />
              <span className="font-extrabold text-37 ml-4">Marico</span>
            </Link>
            <a
              className="text-27 mt-3 block -sm:text-xl"
              href="mailto:info@marico.co"
              target="_blank"
              rel="noreferrer"
            >
              info@marico.co
            </a>
          </div>
          <div className="center text-gray -sm:text-base">
            <ul className="flex items-center gap-x-[38px]">
              <li>
                <Link to={routes.ABOUT} className="hover:text-blue">
                  About
                </Link>
              </li>
              <li>
                <Link to={routes.PRICING} className="hover:text-blue">
                  Pricing
                </Link>
              </li>
              <li>
                <Link to={routes.BLOG} className="hover:text-blue">
                  Blog
                </Link>
              </li>
              <li>
                <Link to={routes.LOGIN} className="hover:text-blue">
                  Sign in
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
